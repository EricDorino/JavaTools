
package fr.dorino.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ToolsTest {

    @BeforeAll
    public static void setUpClass() throws Exception {
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void wordCount() throws Exception {
        System.out.println("wordCount()");
        Reader reader = new StringReader(
                "X3\nX2\nX1\nX3\nX2\nX3");
        HashMap<String, Integer> map = Tools.wordCount(reader);

        Iterator<Map.Entry<String, Integer>> iter = map.entrySet().iterator();
        boolean result = true;
        while (iter.hasNext()) {
            Map.Entry<String, Integer> me = iter.next();
            String key = me.getKey();
            if ("X1".equals(key))
                result = result || me.getValue() == 1;
            else if ("X2".equals(key))
                result = result || me.getValue() == 2;
            else if ("X3".equals(key))
                result = result || me.getValue() == 3;
        }
        assertTrue(result);
    }

    @Test
    public void wordSplit() throws Exception {
        System.out.println("wordSplit()");
        String data = "Hello le monde";
        Reader reader = new StringReader(data);
        Writer writer = new StringWriter();

        Tools.wordSplit(reader, writer, 1);        
        
        assertTrue(
            (data.replace(" ", " " + System.lineSeparator()) + 
            System.lineSeparator()).equals(writer.toString())
        );
    }

    @Test
    public void sentenceSplit() throws Exception {
        System.out.println("sentenceSplit()");
        String[] data = {
          "Phrase 1",
          "Phrase 2",
          "Phrase 3",
          "Phrase 4",
          "Phrase 5"
        };
        String dataAll =
                data[0] + "." +
                data[1] + " ?" +
                data[2] + "!" +
                data[3] + "\r" +
                data[4];
        Reader reader = new StringReader(dataAll);
        Writer writer = new StringWriter();

        Tools.sentenceSplit(reader, writer);

        BufferedReader check = new BufferedReader(new StringReader(writer.toString()));
        String line;
        int i = 0;
        boolean result = true;
        while ((line = check.readLine()) != null) {
            System.out.println("\""+line+"\"");
            result = result || line.equals("Phrase "+i++);
        }

        assertTrue(result);
    }

    @Test
    public void linearRegression1() throws Exception {
        System.out.println("linearRegression1()");

        Tools.Point[] p = new Tools.Point[30];
        for (int i = 0; i < p.length; i++) {
            p[i] = new Tools.Point((double)i, (double)(i*2+1));
        }
        Tools.LinearRegression lr = Tools.linearRegression(p);

        System.out.printf("Regression: %s\n" +
                    "Standard error of beta0:   %f\n" +
                    "Standard error of beta1:   %f\n" +
                    "Residual sum of squares:   %f\n" +
                    "Regression sum of squares: %f\n",
                    lr.toString(),
                    lr.var0(),
                    lr.var1(),
                    lr.rss(),
                    lr.ssr());

        assert(lr.beta0() == 1.0 && lr.beta1() == 2.0);
    }

    @Test
    public void linearRegression2() throws Exception {
        System.out.println("linearRegression2()");

        Tools.Point[] p = new Tools.Point[30];
        for (int i = 0; i < p.length; i++) {
            p[i] = new Tools.Point((double)i, (double)(i*2-1));
        }
        Tools.LinearRegression lr = Tools.linearRegression(p);

        System.out.printf("Regression: %s\n" +
                    "Standard error of beta0:   %f\n" +
                    "Standard error of beta1:   %f\n" +
                    "Residual sum of squares:   %f\n" +
                    "Regression sum of squares: %f\n",
                    lr.toString(),
                    lr.var0(),
                    lr.var1(),
                    lr.rss(),
                    lr.ssr());

        assert(lr.beta0() == -1.0 && lr.beta1() == 2.0);
    }

    @Test
    public void linearRegression3() throws Exception {
        System.out.println("linearRegression3()");

        Tools.Point[] p = new Tools.Point[30];
        for (int i = 0; i < p.length; i++) {
            p[i] = new Tools.Point((double)i, (double)(i*i));
        }
        Tools.LinearRegression lr = Tools.linearRegression(p);

        System.out.printf("Regression: %s\n" +
                    "Standard error of beta0:   %f\n" +
                    "Standard error of beta1:   %f\n" +
                    "Residual sum of squares:   %f\n" +
                    "Regression sum of squares: %f\n",
                    lr.toString(),
                    lr.var0(),
                    lr.var1(),
                    lr.rss(),
                    lr.ssr());

        assert(lr.beta0() > -135.334 && lr.beta0() < -135.333 && lr.beta1() == 29.0);
    }

    @Test
    public void linearRegression4() throws Exception {
        System.out.println("linearRegression4()");

        Random rand = new Random(123456);

        Tools.Point[] p = new Tools.Point[30];
        for (int i = 0; i < p.length; i++) {
            p[i] = new Tools.Point((double)i, rand.nextDouble()*30.0);
        }
        Tools.LinearRegression lr = Tools.linearRegression(p);

        System.out.printf("Regression: %s\n" +
                    "Standard error of beta0:   %f\n" +
                    "Standard error of beta1:   %f\n" +
                    "Residual sum of squares:   %f\n" +
                    "Regression sum of squares: %f\n",
                    lr.toString(),
                    lr.var0(),
                    lr.var1(),
                    lr.rss(),
                    lr.ssr());

        assert(lr.beta0() > 16.648 && lr.beta0() < 16.649 &&
               lr.beta1() > -0.225 && lr.beta0() > -0.224);
    }

    @Test
    public void pivotTable1() throws Exception {
        System.out.println("pivotTable1()");

        class Data implements Tools.PivotData {
            private final String key, value;
            Data(String key, String value) {
                this.key = key;
                this.value = value;
            }
            @Override
            public Object getKey() { return key; }
            @Override
            public Object getValue() { return value; }
        }

        List<Tools.PivotData> list = new LinkedList<>();
        list.add(new Data("k1", "A"));
        list.add(new Data("k1", "B"));
        list.add(new Data("k1", "C"));
        list.add(new Data("k2", "X"));
        list.add(new Data("k3", "Y"));
        list.add(new Data("k1", "A"));
        list.add(new Data("k3", "Y"));

        List<Tools.PivotResult> result = Tools.pivotTable(list, true);

        StringWriter str;
        BufferedWriter writer = new BufferedWriter(str = new StringWriter());

        Iterator<Tools.PivotResult> iter = result.iterator();
        while (iter.hasNext()) {
            Tools.PivotResult r = iter.next();
            writer.write(r.getKey().toString());
            Iterator<Tools.PivotResult.Value> i = r.getValue().iterator();
            while (i.hasNext()) {
                writer.write(" ");
                writer.write(i.next().toString());
            }
            writer.newLine();
        }
        writer.close();
        assert(
            ("k1 A/2 B/1 C/1 X/0 Y/0" + System.lineSeparator() +
             "k2 A/0 B/0 C/0 X/1 Y/0" + System.lineSeparator() +
             "k3 A/0 B/0 C/0 X/0 Y/2" + System.lineSeparator()).equals(str.getBuffer().toString())
        );
    }

    @Test
    public void pivotTable2() throws Exception {
        System.out.println("pivotTable2()");

        class Data implements Tools.PivotData {
            private final String key, value;
            Data(String key, String value) {
                this.key = key;
                this.value = value;
            }
            @Override
            public Object getKey() { return key; }
            @Override
            public Object getValue() { return value; }
        }

        List<Tools.PivotData> list = new LinkedList<>();
        list.add(new Data("k1", "A"));
        list.add(new Data("k1", "B"));
        list.add(new Data("k1", "C"));
        list.add(new Data("k2", "X"));
        list.add(new Data("k3", "Y"));
        list.add(new Data("k1", "A"));
        list.add(new Data("k3", "Y"));

        List<Tools.PivotResult> result = Tools.pivotTable(list, false);

        StringWriter str;
        BufferedWriter writer = new BufferedWriter(str = new StringWriter());

        Iterator<Tools.PivotResult> iter = result.iterator();
        while (iter.hasNext()) {
            Tools.PivotResult r = iter.next();
            writer.write(r.getKey().toString());
            Iterator<Tools.PivotResult.Value> i = r.getValue().iterator();
            while (i.hasNext()) {
                writer.write(" ");
                writer.write(i.next().toString());
            }
            writer.newLine();
        }
        writer.close();
        assert(
            ("k1 A/2 B/1 C/1" + System.lineSeparator() +
             "k2 X/1" + System.lineSeparator() + 
             "k3 Y/2" + System.lineSeparator()).equals(str.getBuffer().toString())
        );
    }

}
