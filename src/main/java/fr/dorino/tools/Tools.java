
package fr.dorino.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Tools {

    /**
     * Count the occurences of every word.
     *
     * @param reader    Input data (\<word\>\n*)
     * @return          a HashMap of words and occurences
     * @throws IOException
     */
    public static HashMap<String, Integer> wordCount(Reader reader)
            throws IOException {
        HashMap<String, Integer> map = new HashMap<>();

        BufferedReader breader = new BufferedReader(reader);
        String line;
        while ((line = breader.readLine()) != null) {
            line = line.replaceAll("\"", "").trim();
            if (line.isEmpty())
                continue;
            Integer count = map.get(line);
            if (count == null) {
                map.put(line, 1);
            } else {
                map.put(line, count + 1);
            }
        }
        breader.close();

        return map;
    }

    /**
     * Split a line into words
     *
     * @param reader    Input data
     * @param writer    Output data
     * @param level
     * @throws IOException
     */
    public static void wordSplit(Reader reader, Writer writer, int level)
                throws IOException {
        if (level <= 0 || level > 10)
            throw new IllegalArgumentException("wordSplit(): 0< level <= 10 failed");
        BufferedReader breader = new BufferedReader(reader);
        BufferedWriter bwriter = new BufferedWriter(writer);
        String line;
        while ((line = breader.readLine()) != null) {
            line = line.replaceAll("\"", "");
            line = line.replace('\'', ' ');
            String[] words = line.split(" ");
            for (int i = 0; i < words.length; i++) {
                if (words[i].isEmpty())
                    continue;
                if (i < words.length-(level-1)) {
                    for (int j = i; j < i+level; j++) {
                        bwriter.write(words[j]);
                        if (j < words.length-1)
                            bwriter.write(" ");
                    }
                    bwriter.newLine();
                }
            }
        }
        breader.close();
        bwriter.close();
    }

    public static void sentenceSplit(Reader reader, Writer writer)
                throws IOException {
            BufferedReader breader = new BufferedReader(reader);
            BufferedWriter bwriter = new BufferedWriter(writer);
            String line;
            while ((line = breader.readLine()) != null) {
                line = line.replaceAll("\"", "");
                line = line.replace('\'', ' ');
                line = line.replace(',', ' ');
                line = line.replace("...", ".");
                line = line.replace("M.", "M ");
                String[] sentences = line.split("[.;:!?\n\r]");
                for (int i = 0; i < sentences.length; i++) {
                    if (sentences[i].isEmpty())
                        continue;
                    bwriter.write(sentences[i].trim());
                    bwriter.newLine();
                }
            }
            breader.close();
            bwriter.close();
    }

    public static class Point {
        public double x, y;
        public Point(double x, double y) { this.x = x; this.y = y; }
        @Override
        public String toString() {
            return String.format("[%f, %f", x, y);
        }
    }

    public static class LinearRegression {
        private final double beta0, beta1, // y = beta1 *x + beta0
                var0,  // Standard error of beta0
                var1,  // Standard error of beta1
                rss,    // Residual sum of square
                ssr;    // Regression sum of square
        LinearRegression(double beta0, double beta1,
                double var0, double var1,
                double rss, double ssr) {
            this.beta0 = beta0;
            this.beta1 = beta1;
            this.var0 = var0;
            this.var1 = var1;
            this.rss = rss;
            this.ssr = ssr;
        }
        public double beta0() { return beta0; }
        public double beta1() { return beta1; }
        public double var0() { return var0; }
        public double var1() { return var1; }
        public double rss() { return rss; }
        public double ssr() { return ssr; }
        @Override
        public String toString() {
            return String.format("y = %f * x %s %f",
                    beta1,
                    beta0 > 0.0 ? "+" : "-",
                    Math.abs(beta0));
        }

    }

    public static LinearRegression linearRegression(Point[] coord) {
        double sumx = 0.0, sumx2 = 0.0, sumy = 0.0;

        for (int i = 0; i < coord.length; i++) {
            sumx += coord[i].x;
            sumx2 += sumx*sumx;
            sumy += coord[i].y;
        }

        double xbar = sumx/coord.length,
               ybar = sumy/coord.length;

        double xxbar = 0.0, yybar = 0.0, xybar = 0.0;

        for (int i = 0; i < coord.length; i++) {
            xxbar += (coord[i].x-xbar)*(coord[i].x-xbar);
            yybar += (coord[i].y-ybar)*(coord[i].y-ybar);
            xybar += (coord[i].x-xbar)*(coord[i].y-ybar);
        }

        double beta1 = xybar/xxbar,
               beta0 = ybar-beta1*xbar;

        int df = coord.length-2;
        double rss = 0.0,   // Residual Sum of Squares
               ssr = 0.0;   // Regression Sum of Squares

        double[] fit = new double[coord.length];
        for (int i = 0; i < coord.length; i++) {
            fit[i] = beta1*coord[i].x+beta0;
            rss += (fit[i]-coord[i].y)*(fit[i]-coord[i].y);
            ssr += (fit[i]-ybar)*(fit[i]-ybar);
        }

        double svar = rss/df,
               svar1 = svar/xxbar,
               //svar0 = svar/coord.length+xbar*xbar*svar1;
               svar0 = svar*sumx2/(coord.length*xxbar);

        return new LinearRegression(beta0, beta1,
                Math.sqrt(svar0), Math.sqrt(svar1),
                rss, ssr);
    }


//    public static abstract class Object { }

//    public static abstract class Object implements Comparable {    }

    public static interface PivotData {
        public Object getKey();
        public Object getValue();
    }

    public static class PivotResult {

        static class Value {
            private final Object value;
            private final int occ;
            Value(Object value, int occ) {
                this.value = value;
                this.occ = occ;
            }
            public Object value() { return value; }
            public int occ() { return occ; }
            @Override
            public String toString() {
                return String.format("%s/%d", value, occ);
            }
        }

        private final Object key;
        private final List<Value> value;

        PivotResult(Object key, List<Value> value) {
            this.key = key;
            this.value = value;
        }

        Object getKey() { return key; }
        List<Value> getValue() { return value; }
    }

    public static List<PivotResult> pivotTable(List<PivotData> input, boolean exhaust) {
        Map<Object, Map<Object, Integer>> map = new HashMap<>();
        Set<Object> set = new HashSet<>();
        List<Object> list = new LinkedList<>();

        Iterator<PivotData> inputIter = input.iterator();
        while (inputIter.hasNext()) {
            PivotData data = inputIter.next();
            set.add(data.getValue());
            Map<Object, Integer> m = map.get(data.getKey());
            if (m == null) {
                m = new HashMap<>();
                m.put(data.getValue(), 1);
                map.put(data.getKey(), m);
            }
            else {
                Integer occ = m.get(data.getValue());
                if (occ == null) {
                    m.put(data.getValue(), 1);
                }
                else {
                    m.put(data.getValue(), occ+1);
                }
            }
        }
        list.addAll(set);
//        Collections.sort(list);

        List<PivotResult> output = new LinkedList<>();

        Iterator<Map.Entry<Object, Map<Object, Integer>>> outputIter = map.entrySet().iterator();
        while (outputIter.hasNext()) {
            Map.Entry<Object, Map<Object, Integer>> me = outputIter.next();

            Iterator<Object> valIter = exhaust ?
                    list.iterator():
                    me.getValue().keySet().iterator();
            List<PivotResult.Value> values = new LinkedList<>();
            while (valIter.hasNext()) {
                Object val = valIter.next();
                int occ = me.getValue().getOrDefault(val, 0);
                values.add(new PivotResult.Value(val, occ));
            }
            output.add(new PivotResult(me.getKey(), values));
        }
        return output;
    }

}
